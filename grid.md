# Selenium Grid

## Stack

```
---
version: '3.4'

services:
  firefox:
     image: selenium/node-firefox:latest
     entrypoint: bash -c 'SE_OPTS="-host $$HOSTNAME -port 5555" /opt/bin/entry_point.sh'
     environment:
       - HUB_PORT_4444_TCP_ADDR=hub
       - HUB_PORT_4444_TCP_PORT=4444
     networks:
       - jenkins_default
       - default
  chrome:
     image: selenium/node-chrome:latest
     entrypoint: bash -c 'SE_OPTS="-host $$HOSTNAME -port 5555" /opt/bin/entry_point.sh'
     environment:
       - HUB_PORT_4444_TCP_ADDR=hub
       - HUB_PORT_4444_TCP_PORT=4444
     networks:
       - jenkins_default
       - default
  hub:
    image: selenium/hub:latest
    networks:
    - jenkins_default
    - default
    ports:
    - "4444:4444"

networks:
  jenkins_default:
    external: true

```

## Robot Resources

```
*** Settings ***
Library        SeleniumLibrary

*** Variables ***
${SERVER}      https://topikettunen.com
${GRID_URL}    http://hub:4444/wd/hub
${BROWSER}     Firefox
${DELAY}       5

*** Keywords ***
Suite Setup
    Open Browser  ${SERVER}    ${BROWSER}    None    ${GRID_URL}    version:ANY
    Set Selenium Speed  ${DELAY}

Suite Teardown
    Close Browser
```
