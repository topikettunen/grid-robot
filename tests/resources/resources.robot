*** Settings ***
Library        SeleniumLibrary

*** Variables ***
${SERVER}      https://kettunen.io
${GRID_URL}    http://localhost:4444/wd/hub
${BROWSER}     Chrome
${DELAY}       5

*** Keywords ***
Suite Setup
    Open Browser  ${SERVER}    ${BROWSER}    None    ${GRID_URL}    version:ANY
    Set Selenium Speed  ${DELAY}

Suite Teardown
    Close Browser

User should see appropriate title
    [Arguments]     ${image_id}
    Wait Until Element Is Visible    xpath=//*[@id="static"]
    Element Should Contain           xpath=//*[@id="static"]    ${image_id}
