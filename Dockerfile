FROM ubuntu:latest

RUN apt-get update && apt-get install -y \
    python3 \
    python3-pip

RUN pip3 install robotframework \
    robotframework-seleniumlibrary

COPY tests tests

# Remember entrypoint on your own containers!!!
